#include <stdio.h>
#include <math.h>
#define PI 3.14159
struct Point{
    float x;
    float y;
};
typedef struct Point point;
struct Line{
    point p1;
    point p2;
};
typedef struct Line line;
line input() {
    line a;
    scanf("%f",&a.p1.x);
    scanf("%f",&a.p1.y);
    scanf("%f",&a.p2.x);
    scanf("%f",&a.p2.y);
    return a;
}
line compute(line m) {
   line k;
   point mid;
   mid.x=(m.p1.x+m.p2.x)/2;
   mid.y=(m.p1.y+m.p2.y)/2; 
   float ra=sqrt(pow(m.p1.x-mid.x,2)+pow(m.p1.y-mid.y,2));//length of line
   float theta=atan((m.p1.y-m.p2.y)/(m.p1.x-m.p2.x));//inclination of line
   k.p1.x=(ra*cos(theta+PI/2))+mid.x;//   coordinates after 
   k.p1.y=(ra*sin(theta+PI/2))+mid.y;//   rotating by 90
   k.p2.x=(ra*cos(theta + (3*PI/2)))+mid.x;
   k.p2.y=(ra*sin(theta + (3*PI/2)))+mid.y;
   return k;
}
void output (line l, line k) {
    printf("Rotating (%f,%f)(%f,%f) yields (%f,%f)(%f,%f) \n",l.p1.x,l.p1.y,l.p2.x,l.p2.y,k.p1.x,k.p1.y,k.p2.x,k.p2.y);
}
int main () {
    int n;
    scanf("%d",&n);
    line l1[n], l2[n];
    for(int i=0;i<n;i++) {
        l1[i]=input();
        l2[i]=compute(l1[i]);
    }
    for(int j=0;j<n;j++) {
        output(l1[j], l2[j]);
    }
    return 0;
}
