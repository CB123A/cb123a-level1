#include <stdio.h>
struct Fraction{
    int x;
    int y;
};
typedef struct Fraction frac;
int gcd (int a,int b) {
	int t;
	while (b>0) {
		t=b;
		b=a%b;
		a=t;
	}
	return a;
}
int input() {
    int a=0;
    scanf("%d",&a);
    return a;
}
frac compute(frac m[], int s) {
    frac v;
    v.x=0;
    v.y=1;
    for(int i=0;i<s;i++) {
        v.x+=m[i].x*v.y+m[i].y*v.x;
        v.y*=v.y*m[i].y;
    }
    return v;
}
void output (frac fr[], frac k, int l) {
    for(int i=0;i<l-1;i++){
        printf("%d/%d +",fr[i].x,fr[i].y);
    }
    printf("%d/%d = %d/%d",fr[l-1].x,fr[l-1].y,k.x,k.y);
}
int main() {
    int n,l;
    frac k;
    scanf("%d",&n);
    for(int j=0;j<n;j++) {
        scanf("%d",&l);
        frac f[l];
        for(int i=0;i<l;i++) {
            f[i].x=1;
            f[i].y=input();
        }
        k=compute(f,l);
        int g=gcd(k.x,k.y);
        k.x/=g;
        k.y/=g;
        output(f,k,l);
    }
    return 0;
}