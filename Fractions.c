
#include<stdio.h>
struct Fraction {
	int x;
	int y;
};
typedef struct Fraction frac;
frac input() {
	frac a;
	printf("Enter numerator:");
	scanf("%d",&a.x);
	printf("Enter denominator:");
	scanf("%d",&a.y);
	return a;
}
int gcd (int a,int b) {
	int t;
	while (b>0) {
		t=b;
		b=a%b;
		a=t;
	}
	return a;
}
frac calculate(frac m, frac n) {
	frac p;
	p.x=(m.x*n.y)+(m.y*n.x);
	p.y=m.y*n.y;
	int g= gcd(p.x,p.y);
	p.x/=g;
	p.y/=g;
	return p;
}
void output( frac f1, frac f2, frac f0) {
	 printf("Sum of %d/%d and %d/%d is %d/%d",f1.x,f1.y,f2.x,f2.y,f0.x,f0.y);
 }
int main() {
	frac b,p,q;
	p=input();
	q=input();
	b=calculate(p,q);
	output(p,q,b);
	return 0;
}
