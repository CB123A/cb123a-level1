#include <stdio.h>
#include <math.h>
struct point {
float x;
float y;
};
typedef struct point Point;
float input() {
  float a=0.0;
  scanf("%f",&a);
  return a;
}
float calculate(Point p11, Point p22) {
  float dist=sqrt((pow(p11.x,2)+pow(p11.y,2))+(pow(p22.x,2)+pow(p22.y,2)));
  return dist;
}
void display(float dis) {
  printf( "distance between points are %f",dis);
}
int main () {
Point p1;
Point p2;
p1.x=input();
p1.y=input();
p2.x=input();
p2.y=input();
float distance=calculate(p1,p2);
display(distance);
return 0;
}


