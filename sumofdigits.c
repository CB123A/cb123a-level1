#include <stdio.h> 
int input() {
	int a=0;
	printf("Enter number\n");
	scanf("%d",&a);
	return a;
}
int sumofd(int b) {
	int sum=0;
	while(b>1) {
		sum+=(b%10);
		b=b/10;
	}
	return sum;
}
int main() {
	int x=input();
	int sums=sumofd(x);
	printf("The sum of digits is %d",sums);
	return 0;
}