#include <stdio.h>
struct fraction {
	int x;
	int y;
};
typedef struct fraction frac;
int gcd (int a,int b) {
	int t;
	while (b>0) {
		t=b;
		b=a%b;
		a=t;
	}
	return a;
}
frac input() {
	frac f1;
	printf("Enter numerator:");
	scanf("%d",&f1.x);
	printf("Enter denominator:");
	scanf("%d",&f1.y);
	return f1;
}
frac compute(frac f[], int s) {
	frac k;
	k.x=f[0].x;
	k.y=f[0].y;
	for (int j=1;j<s;j++) {
		k.x=(f[j].x*k.y)+(f[j].y*k.x);
		k.y*=f[j].y;
	}
	int g=gcd(k.x,k.y);
	k.x/=g;
	k.y/=g;
	return k;
}
	
void output(frac fs[], int n, frac f0) {
	for (int i=0;i<n-1;i++) {
		printf("%d/%d + ",fs[i].x,fs[i].y);
	}
	printf("%d/%d",fs[n-1].x,fs[n-1].y);
	printf(" is %d/%d",f0.x,f0.y);
} 
int main () {
	int n=0;
	printf("Enter number of fractions:");
	scanf("%d",&n);
	frac f[n];
	frac fr;
	for (int i=0;i<n;i++) {
		f[i]=input();
	}
	fr=compute(f,n);
	output(f,n,fr);
	return 0;
}