#include <stdio.h>
#include <math.h>
struct Point{
    float x;
    float y;
};
typedef struct Point point;
struct Data{
	point c,p;
	float rad;
};
typedef struct Data data;
data input() {
    data a;
    scanf("%f",&a.c.x);
    scanf("%f",&a.c.y);
    scanf("%f",&a.p.x);
    scanf("%f",&a.p.y);
    scanf("%f",&a.rad);
    return a;
}
point compute(data m) {
   point q0;
   float ra=sqrt(pow(m.p.x-m.c.x,2)+pow(m.p.y-m.c.y,2));//length of line
   float theta=atan((m.p.y-m.c.y)/(m.p.x-m.c.x));//inclination of line
   q0.x=(ra*cos(theta+m.rad))+m.c.x;//   coordinates after 
   q0.y=(ra*sin(theta+m.rad))+m.c.y;//   rotating by rad
   return q0;
}
void output (data k, point r) {
    printf("Rotating (%f,%f) %f radians about (%f,%f) yields (%f,%f) \n",k.p.x,k.p.y,k.rad,k.c.x,k.c.y,r.x,r.y);
}
int main () {
    int n;
    scanf("%d",&n);
    data d[n];
    point q[n];
    for(int i=0;i<n;i++) {
        d[i]=input();
        q[i]=compute(d[i]);
    }
    for(int j=0;j<n;j++) {
        output(d[j], q[j]);
    }
    return 0;
}
